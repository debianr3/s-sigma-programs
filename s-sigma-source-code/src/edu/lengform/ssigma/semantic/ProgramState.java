/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic;

import edu.lengform.ssigma.syntax.SigmaPSymbols;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class ProgramState {
    private static final VarsComparator COMPARATOR = new VarsComparator();

    private final HashMap<Integer, Integer> numsMemTable;
    private final HashMap<Integer, String> wordsMemTable;

    public ProgramState(int numsMemSize, int wordsMemSize) {
        numsMemTable = new HashMap<Integer, Integer>();
        wordsMemTable = new HashMap<Integer, String>();
    }

    public int nums_getMem(int memId) {
        if (!numsMemTable.containsKey(memId)) {
            numsMemTable.put(memId, 0);
        }
        return numsMemTable.get(memId);
    }

    public void nums_setMem(int memId, int value) {
        numsMemTable.put(memId, value);
    }

    public void printState() {
        if (true) {
            System.out.println("    Numeric Memory: " + exposeNumMem());
            System.out.println("    Alphabetic Memory: " + exposeWordsMem());
        }
    }

    public String words_getMem(int memId) {
        if (!wordsMemTable.containsKey(memId)) {
            wordsMemTable.put(memId, "");
        }
        return wordsMemTable.get(memId);
    }

    public void words_setMem(int memId, String value) {
        wordsMemTable.put(memId, value);
    }

    public ArrayList<String> exposeNumMem() {
        ArrayList<String> mem = new ArrayList<String>(numsMemTable.size());
        for (Integer key : numsMemTable.keySet()) {
            mem.add("N" + key + "= " + numsMemTable.get(key));
        }
        Collections.sort(mem, COMPARATOR);
        return mem;
    }

    public ArrayList<String> exposeWordsMem() {
        ArrayList<String> mem = new ArrayList<String>(wordsMemTable.size());
        for (Integer key : wordsMemTable.keySet()) {
            String value = wordsMemTable.get(key);
            if (value == null || value.length() == 0) {
                value = "" + SigmaPSymbols.EPSILON.getSymbol();
            }
            mem.add("P" + key + "= " + value);
        }
        Collections.sort(mem, COMPARATOR);
        return mem;
    }

    private static final class VarsComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            int i1 = o1.indexOf('=');
            int i2 = o2.indexOf('=');

            int delta = i1 - i2;
            if (delta != 0) {
                return delta;
            }
            return o1.substring(0, i1).compareTo(o2.substring(0, i2));
        }
    }
}
