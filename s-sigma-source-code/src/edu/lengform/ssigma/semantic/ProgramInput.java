/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.semantic;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Take the input in string like N1=10, N2=0, ..., P1=text, ..., 
 * and prepare to set in memory.
 */
public class ProgramInput {

    public final int maxNumsId;
    public final int maxWordsId;
    private final HashMap<String, Integer> numsInput = new HashMap<String, Integer>();
    private final HashMap<String, String> wordsInput = new HashMap<String, String>();

    public ProgramInput(String input) {

        Pattern pattern = Pattern.compile("\\s*(N[0-9]+)\\s*=\\s*([0-9]+)\\s*");
        Matcher m = pattern.matcher(input);

        int maxNums = 0;
        while (m.find()) {
            final String var = m.group(1);
            final String value = m.group(2);
            numsInput.put(var, Integer.parseInt(value));
            maxNums = Math.max(maxNums, SemanticHelper.GetMemID(var));
        }
        maxNumsId = maxNums;

        pattern = Pattern.compile("\\s*(P[0-9]+)\\s*=\\s*(.*)\\s*");
        m = pattern.matcher(input);

        int maxWords = 0;
        while (m.find()) {
            final String var = m.group(1);
            final String value = m.group(2);
            wordsInput.put(var, value);
            maxWords = Math.max(maxWords, SemanticHelper.GetMemID(var));
        }

        maxWordsId = maxWords;
    }

    public void setInMemory(ProgramState state) {
        for (String var : numsInput.keySet()) {
            final int memId = SemanticHelper.GetMemID(var);
            final int value = numsInput.get(var);

            state.nums_setMem(memId, value);
        }
        for (String var : wordsInput.keySet()) {
            final int memId = SemanticHelper.GetMemID(var);
            final String value = wordsInput.get(var);

            state.words_setMem(memId, value);
        }
    }
}
