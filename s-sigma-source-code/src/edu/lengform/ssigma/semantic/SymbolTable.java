/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class SymbolTable {

    private HashMap<String, Integer> table = new HashMap<String, Integer>();

    public boolean containsVariable(String name) {
        return table.containsKey(name);
    }

    public int get(String name) {
        return table.get(name);
    }

    public int getSize() {
        return table.size();
    }

    public Set<String> keySet() {
        return table.keySet();
    }

    public ArrayList<String> keySet(String keyPrefix) {
        ArrayList<String> result = new ArrayList<String>();
        for (String key : table.keySet()) {
            if (key.startsWith(keyPrefix)) {
                result.add(key);
            }
        }
        Collections.sort(result);

        return result;
    }

    public void put(String name) {
        if (!table.containsKey(name)) {
            table.put(name, SemanticHelper.GetMemID(name));
        }
    }
}
