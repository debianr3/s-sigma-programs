/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic.instructions;

public class InsConstants {
    public static final int INS_NUMERIC_ASSIGNMENT = 1;
    public static final int INS_ALPHABETIC_ASSIGNMENT = 2;
    public static final int INS_GOTO = 3;
    public static final int INS_IF = 4;
    public static final int INS_SKIP = 5;
    public static final int INS_MACRO = 999999;

    public static final int OP_ASSIGN_VAR = 111;

    public static final int OP_PLUS_ONE = 222;
    public static final int OP_MINUS_ONE = 223;
    public static final int OP_ASSIGN_CERO = 224;

    public static final int OP_RM_FIRST = 331;
    public static final int OP_CONCAT = 332;
    public static final int OP_ASSIGN_EPSILON = 333;

    public static final int OP_NOT_CERO = 441;
    public static final int OP_BEGINS = 442;

    public static final int CONST_NO_LABEL = 0;
    public static final int CONST_VERSION = 1;
}
