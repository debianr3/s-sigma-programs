/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic.instructions;

import edu.lengform.ssigma.syntax.SigmaPSymbols;

public class NumericAssignment extends BasicInstruction {
    private final int varId;
    private final int op;
    private final int otherVarId;

    public NumericAssignment(int varId, int op, int otherVarId) {
        this.varId = varId;
        this.op = op;
        this.otherVarId = otherVarId;
    }

    private static int minusOne(int x) {
        return Math.max(0, x - 1);
    }

    @Override
    public void execute() {
        switch (op) {
            case InsConstants.OP_ASSIGN_CERO:
                Program.instance.state.nums_setMem(varId, 0);
                break;
            case InsConstants.OP_PLUS_ONE:
                Program.instance.state.nums_setMem(varId, Program.instance.state.nums_getMem(otherVarId) + 1);
                break;
            case InsConstants.OP_MINUS_ONE:
                Program.instance.state.nums_setMem(varId, minusOne(Program.instance.state.nums_getMem(otherVarId)));
                break;
            case InsConstants.OP_ASSIGN_VAR:
                Program.instance.state.nums_setMem(varId, Program.instance.state.nums_getMem(otherVarId));
                break;
        }
        Program.instance.incrementIP();
    }

    @Override
    public String toPseudoProgram(int numsOffset, int wordsOffset) {
        return toString(true, numsOffset, wordsOffset);
    }

    @Override
    public String toString() {
        return toString(false, 0, 0);
    }

    private String toString(boolean isPseudoProgram, int numsOffset, int wordsOffset) {
        final String var = isPseudoProgram ? "V" : "N";

        StringBuilder sb = new StringBuilder(var + (varId + numsOffset) + SigmaPSymbols.OP_ASIGN.getSymbol());

        switch (op) {
            case InsConstants.OP_ASSIGN_CERO:
                sb.append("0");
                break;
            case InsConstants.OP_PLUS_ONE:
                sb.append(var + (otherVarId + numsOffset) + " + 1");
                break;
            case InsConstants.OP_MINUS_ONE:
                sb.append(var + (otherVarId + numsOffset) + SigmaPSymbols.OP_MINUS.getSymbol() + " 1");
                break;
            case InsConstants.OP_ASSIGN_VAR:
                sb.append(var + (otherVarId + numsOffset));
                break;
        }

        return sb.toString();
    }

    @Override
    public int getVariableId() {
        return Math.max(varId, otherVarId);
    }
}
