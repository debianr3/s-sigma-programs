/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic.instructions;

public abstract class BasicInstruction {

    public abstract void execute();


    /**
     * Convert this instruction to a pseudo instruction of a pseudo program that
     * can be saved as a macro.
     *
     * @param numsOffset numeric variables offset to move the variables
     * @param wordsOffset alphabetic variables offset to move the variables
     * @return
     */
    public abstract String toPseudoProgram(int numsOffset, int wordsOffset);

    /**
     * Get the max variable (numeric, alphabetic) id. For example: the max id
     * for instruction "A12 N3 = N5" will be 5.
     *
     * @return max id.
     */
    public abstract int getVariableId();
}
