/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.semantic.instructions;

import edu.lengform.ssigma.syntax.SigmaPSymbols;

public class AlphabeticAssignment extends BasicInstruction {
    private final int varId;
    private final int op;
    private final int otherVarId;
    private final char symbol;

    public AlphabeticAssignment(int varId, int op, int otherVarId, char symbol) {
        super();
        this.varId = varId;
        this.op = op;
        this.otherVarId = otherVarId;
        this.symbol = symbol;
    }

    private static String concat(String word, char symbol) {
        if (word == null) {
            return "" + symbol;
        } else {
            return word + symbol;
        }
    }

    private static String removeFirst(String word) {
        if (word != null && word.length() > 0) {
            return word.substring(1, word.length());
        }
        return null;
    }

    @Override
    public void execute() {
        switch (op) {
            case InsConstants.OP_ASSIGN_EPSILON:
                Program.instance.state.words_setMem(varId, null);
                break;
            case InsConstants.OP_CONCAT:
                Program.instance.state.words_setMem(varId, concat(Program.instance.state.words_getMem(otherVarId), symbol));
                break;
            case InsConstants.OP_RM_FIRST:
                Program.instance.state.words_setMem(varId, removeFirst(Program.instance.state.words_getMem(otherVarId)));
                break;
            case InsConstants.OP_ASSIGN_VAR:
                Program.instance.state.words_setMem(varId, Program.instance.state.words_getMem(otherVarId));
                break;
        }

        Program.instance.incrementIP();
    }

    @Override
    public String toPseudoProgram(int numsOffset, int wordsOffset) {
        return toString(true, numsOffset, wordsOffset);
    }

    @Override
    public String toString() {
        return toString(false, 0, 0);
    }

    private String toString(boolean isPseudoProgram, int numsOffset, int wordsOffset) {
        final String var = isPseudoProgram ? "W" : "P";

        StringBuilder sb = new StringBuilder(var + (varId + wordsOffset) + SigmaPSymbols.OP_ASIGN.getSymbol());

        switch (op) {
            case InsConstants.OP_ASSIGN_EPSILON:
                sb.append(SigmaPSymbols.EPSILON.getSymbol());
                break;
            case InsConstants.OP_CONCAT:
                sb.append(var + (otherVarId + wordsOffset) + " . " + symbol);
                break;
            case InsConstants.OP_RM_FIRST:
                sb.append(SigmaPSymbols.OP_DEL_FIRST_CHAR.getSymbol() + var + (otherVarId + wordsOffset));
                break;
            case InsConstants.OP_ASSIGN_VAR:
                sb.append(var + (otherVarId + wordsOffset));
                break;
        }

        return sb.toString();
    }

    @Override
    public int getVariableId() {
        return Math.max(varId, otherVarId);
    }
}
