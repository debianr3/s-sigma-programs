/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic.instructions;

public class Instruction {

    public final int number;
    public final int label;

    private final BasicInstruction basicIns;

    public Instruction(int number, int label, BasicInstruction basicIns) {
        super();
        this.number = number;
        this.label = label;
        this.basicIns = basicIns;
    }

    public void execute() {
        basicIns.execute();
    }

    /**
     * Convert this instruction to a pseudo instruction of a pseudo program that
     * can be saved as a macro.
     *
     * @param numsOffset
     *            numeric variables offset to move the variables
     * @param wordsOffset
     *            alphabetic variables offset to move the variables
     * @return
     */
    public String toPseudoProgram(int numsOffset, int wordsOffset) {
        if (label != 0) {
            return "A" + label + " " + basicIns.toPseudoProgram(numsOffset, wordsOffset);
        }
        return basicIns.toPseudoProgram(numsOffset, wordsOffset);
    }

    /**
     * Get the max variable (numeric, alphabetic) id. For example: the max id
     * for instruction "A12 N3 = N5" will be 5.
     *
     * @return max id.
     */
    public int getVariableId() {
        return basicIns.getVariableId();
    }

    @Override
    public String toString() {
        if (label != 0) {
            return "L" + label + " " + basicIns.toString();
        }
        return basicIns.toString();
    }
}
