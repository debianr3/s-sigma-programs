/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic.instructions;

import edu.lengform.ssigma.semantic.CircularBuffer;
import edu.lengform.ssigma.semantic.ProgramInput;
import edu.lengform.ssigma.semantic.ProgramState;

import java.util.ArrayList;
import java.util.HashMap;

public class Program implements Runnable {
    public static Program instance;

    public final ProgramState state;

    private final HashMap<Integer, Integer> labelInstructionTable;
    private final ProgramInput input;
    private ArrayList<Instruction> instructions;
    private int np;
    private int ip;
    private int steeps = -1;

    public Program(HashMap<Integer, Integer> labelInstructionTable, ArrayList<Instruction> instructions,
                   int numsMemSize, int wordsMemSize, ProgramInput input) {

        this.instructions = instructions;
        this.labelInstructionTable = labelInstructionTable;
        this.input = input;
        np = instructions.size();
        state = new ProgramState(Math.max(numsMemSize, input.maxNumsId), Math.max(wordsMemSize, input.maxWordsId));

        instance = this;
    }

    public int getIP() {
        return ip;
    }

    public void incrementIP() {
        ip++;
    }

    @Override
    public void run() {
        if (input != null) {
            input.setInMemory(state);
        }

        final int totalSteps = steeps;
        ip = 0;

        System.out.println("-------- Start Trace: --------");
        while (ip < np && steeps != 0) {
            if (Thread.interrupted()) {
                break;
            }
            if (totalSteps < 0) {
                System.out.println("  > Run step: " + (-steeps) + ": " + instructions.get(ip).toString());
            } else {
                System.out.println("  > Run step: " + (1 + totalSteps - steeps) + ": "
                        + instructions.get(ip).toString());
            }

            instructions.get(ip).execute();
            steeps--;
        }
        System.out.println("-------- End Trace: --------");
    }

    public void setIPForLabel(int labelId) {
        ip = labelInstructionTable.get(labelId) - 1;
    }

    public void setSteeps(int steeps) {
        this.steeps = steeps;
    }

    public CircularBuffer buildScreenshotBuffer(int maxScreenshots) {
        return new CircularBuffer(maxScreenshots, new ArrayList<Instruction>(instructions));
    }

    public int getVariableId() {
        int max = 1;
        for (Instruction ins : instructions) {
            max = Math.max(max, ins.getVariableId());
        }
        return max;
    }

    public String toPseudoProgramString(int numsOffset, int wordsOffset) {
        StringBuilder sb = new StringBuilder();
        for (Instruction ins : instructions) {
            sb.append(ins.toPseudoProgram(numsOffset, wordsOffset));
            sb.append('\n');
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Instruction ins : instructions) {
            sb.append(ins.toString());
            sb.append('\n');
        }
        return sb.toString();
    }
}
