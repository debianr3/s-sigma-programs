/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.semantic.instructions;

import edu.lengform.ssigma.syntax.SigmaPSymbols;

public class ConditionalExpression {
    private final int varId;
    private final int op;
    private final char symbol;

    public ConditionalExpression(int varId, int op) {
        this(varId, op, ' ');
    }

    public ConditionalExpression(int varId, int op, char symbol) {
        super();
        this.varId = varId;
        this.op = op;
        this.symbol = symbol;
    }

    private static boolean beginsWith(String word, char symbol) {
        if (word == null || word.length() < 1) {
            return false;
        }
        return word.charAt(0) == symbol;
    }

    public boolean eval() {
        boolean eval = false;
        switch (op) {
            case InsConstants.OP_NOT_CERO:
                eval = Program.instance.state.nums_getMem(varId) != 0;
                break;
            case InsConstants.OP_BEGINS:
                eval = beginsWith(Program.instance.state.words_getMem(varId), symbol);
                break;
        }

        return eval;
    }

    /**
     * Convert this instruction to a pseudo instruction of a pseudo program that
     * can be saved as a macro.
     *
     * @param numsOffset
     *            numeric variables offset to move the variables
     * @param wordsOffset
     *            alphabetic variables offset to move the variables
     * @return
     */
    public String toPseudoProgram(int numsOffset, int wordsOffset) {
        return toString(true, numsOffset, wordsOffset);
    }

    @Override
    public String toString() {
        return toString(false, 0, 0);
    }

    private String toString(boolean isPseudoProgram, int numsOffset, int wordsOffset) {
        final String varNum = isPseudoProgram ? "V" : "N";
        final String varWord = isPseudoProgram ? "W" : "P";

        StringBuilder sb = new StringBuilder();
        switch (op) {
            case InsConstants.OP_NOT_CERO:
                sb.append(varNum + (varId + numsOffset) + SigmaPSymbols.OP_NO_EQ.getSymbol() + 0);
                break;
            case InsConstants.OP_BEGINS:
                sb.append(varWord + (varId + wordsOffset) + " BEGINS " + symbol);
                break;
        }

        return sb.toString();
    }

    public int getVariableId() {
        return varId;
    }
}
