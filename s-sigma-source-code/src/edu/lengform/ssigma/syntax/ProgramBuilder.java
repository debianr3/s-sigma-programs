/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax;

import edu.lengform.ssigma.macro.MacroDatabase;
import edu.lengform.ssigma.macro.MacrosLoader;
import edu.lengform.ssigma.semantic.ProgramInput;
import edu.lengform.ssigma.semantic.instructions.InsConstants;
import edu.lengform.ssigma.semantic.instructions.Instruction;
import edu.lengform.ssigma.semantic.instructions.Program;
import edu.lengform.ssigma.syntax.program.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class ProgramBuilder {

    public static Program buildProgram(ParsedProgram parsedProgram,
                                       ProgramInput programInput, File macrosDatabaseDir) {

        final AvailableIdentifiers ids = StaticAnalizer.analize(parsedProgram);

        MacroDatabase macroDatabase = null;
        if (macrosDatabaseDir != null) {
            try {
                macroDatabase = loadMacrosDatabase(macrosDatabaseDir);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        preprocess(macroDatabase, parsedProgram, ids);

        StaticAnalizer.checkGotoLabels(parsedProgram);

        return createProgram(parsedProgram, programInput);
    }

    private static MacroDatabase loadMacrosDatabase(File macrosDatabaseDir) {
        return new MacrosLoader(macrosDatabaseDir).loadMacros();
    }

    private static void preprocess(MacroDatabase macroDatabase,
                                   ParsedProgram parsedProgram, AvailableIdentifiers ids) {

        ArrayList<ParsedInstruction> newInstructions = new ArrayList<ParsedInstruction>();
        for (ParsedInstruction parsedIns : parsedProgram.getInstructions()) {
            if (parsedIns.basicInstruction.type != InsConstants.INS_MACRO) {
                newInstructions.add(parsedIns);
                continue;
            }
            if (macroDatabase == null) {
                throw new RuntimeException("Error: Macro database not loaded.");
            }

            // Instantiate macro
            final ParsedMacroInstance macroInstance = (ParsedMacroInstance) parsedIns.basicInstruction;
            macroInstance.instantiate(macroDatabase, newInstructions, ids);
        }

        parsedProgram.replaceInstructions(newInstructions);
    }

    private static Program createProgram(ParsedProgram parsedProgram,
                                         ProgramInput programInput) {
        ArrayList<Instruction> insList = new ArrayList<Instruction>();

        for (ParsedInstruction parsedIns : parsedProgram.getInstructions()) {
            insList.add(new Instruction(parsedIns.getInsNumber(),
                    parsedIns.intlabel, parsedIns.basicInstruction
                    .buildProgramInstruction()));
        }

        final int numsMemSize = Math.max(ParsedNumericAssignment.maxVarMemId,
                ParsedConditionalExpression.maxNumsVarMemId);

        final int wordsMemSize = Math.max(
                ParsedAlphabeticAssignment.maxVarMemId,
                ParsedConditionalExpression.maxWordsVarMemId);

        HashMap<Integer, Integer> labelInstructionTable = new HashMap<Integer, Integer>();

        for (Instruction ins : insList) {
            if (!labelInstructionTable.containsKey(ins.label)) {
                labelInstructionTable.put(ins.label, ins.number);
            }
        }

        if (programInput == null) {
            programInput = new ProgramInput("");
        }

        Program program = new Program(labelInstructionTable, insList,
                numsMemSize, wordsMemSize, programInput);

        System.out
                .println("//----------------------------- BEGIN PROGRAM --------------------------//");
        System.out.println(program.toString());
        System.out
                .println("//----------------------------- END PROGRAM --------------------------//");

        return program;
    }
}
