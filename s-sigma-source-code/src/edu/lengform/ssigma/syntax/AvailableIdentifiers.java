/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax;

import java.util.HashMap;

public class AvailableIdentifiers {
    private final HashMap<String, String> labelsReplacements = new HashMap<String, String>();
    private final HashMap<String, String> numsReplacements = new HashMap<String, String>();
    private final HashMap<String, String> wordsReplacements = new HashMap<String, String>();
    private int numsVarId;
    private int wordsVarId;
    private int labelsVarId;

    public AvailableIdentifiers(int numsVarId, int wordsVarId, int labelsVarId) {
        this.numsVarId = numsVarId;
        this.wordsVarId = wordsVarId;
        this.labelsVarId = labelsVarId;
    }

    private int useNumVar() {
        return ++numsVarId;
    }

    private int useWordVar() {
        return ++wordsVarId;
    }

    private int useLabel() {
        return ++labelsVarId;
    }

    public String getReplaceForLabel(String label) {
        if (labelsReplacements.containsKey(label)) {
            return labelsReplacements.get(label);
        }

        final String replace = "L" + useLabel();
        labelsReplacements.put(label, replace);
        return replace;
    }

    public String getReplaceForNumVar(String var) {
        if (numsReplacements.containsKey(var)) {
            return numsReplacements.get(var);
        }

        final String replace = "N" + useNumVar();
        numsReplacements.put(var, replace);
        return replace;
    }

    public String getReplaceForWordVar(String var) {
        if (wordsReplacements.containsKey(var)) {
            return wordsReplacements.get(var);
        }

        final String replace = "P" + useWordVar();
        wordsReplacements.put(var, replace);
        return replace;
    }

    public void setReplacementForNumVar(String numVar, String replacement) {
        if (!numsReplacements.containsKey(numVar)) {
            numsReplacements.put(numVar, replacement);
        }
    }

    public void setReplacementForWordVar(String numVar, String replacement) {
        if (!wordsReplacements.containsKey(numVar)) {
            wordsReplacements.put(numVar, replacement);
        }
    }

    public void setReplacementForLabel(String label, String replacement) {
        if (!labelsReplacements.containsKey(label)) {
            labelsReplacements.put(label, replacement);
        }
    }

    @Override
    public String toString() {
        return "AvailableIdentifiers [labelsReplacements=" + labelsReplacements
                + ", numsReplacements=" + numsReplacements
                + ", wordsReplacements=" + wordsReplacements + "]";
    }

    public void clearRules() {
        numsReplacements.clear();
        wordsReplacements.clear();
        labelsReplacements.clear();
    }
}
