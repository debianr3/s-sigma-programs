/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax;

import edu.lengform.ssigma.semantic.SemanticHelper;
import edu.lengform.ssigma.semantic.SymbolTable;
import edu.lengform.ssigma.semantic.instructions.InsConstants;
import edu.lengform.ssigma.syntax.program.*;

/**
 * - Check if a label at the end of a goto exist at beginning of an instruction.
 * - Generate table of symbols.
 */
public class StaticAnalizer {

    public static AvailableIdentifiers analize(ParsedProgram parsedProgram)
            throws RuntimeException {

        int maxLabelsVarId = checkGotoLabels(parsedProgram);
        int maxNumsVarId = parsedProgram.getMaxNumsVarMemId();
        int maxWordsVarId = parsedProgram.getMaxWordsVarMemId();
        for (ParsedInstruction parsedIns : parsedProgram.getInstructions()) {
            if (parsedIns.basicInstruction.type != InsConstants.INS_MACRO) {
                continue;
            }
            ParsedMacroInstance parsedMacroInstance = ((ParsedMacroInstance) parsedIns.basicInstruction);
            maxNumsVarId = Math.max(maxNumsVarId,
                    parsedMacroInstance.macroName.getMaxNumsVar());
            maxWordsVarId = Math.max(maxWordsVarId,
                    parsedMacroInstance.macroName.getMaxWordsVar());
            maxLabelsVarId = Math.max(maxLabelsVarId,
                    parsedMacroInstance.macroName.getMaxLabelsVar());
        }

        return new AvailableIdentifiers(maxNumsVarId, maxWordsVarId,
                maxLabelsVarId);
    }

    public static int checkGotoLabels(ParsedProgram parsedProgram) {
        int maxLabelsVarId = 0;

        final SymbolTable symbolTable = new SymbolTable();

        // Fill symbol table
        for (ParsedInstruction parsedIns : parsedProgram.getInstructions()) {
            if (parsedIns.label != null
                    && !symbolTable.containsVariable(parsedIns.label)) {
                symbolTable.put(parsedIns.label);
                maxLabelsVarId = Math.max(maxLabelsVarId,
                        SemanticHelper.GetMemID(parsedIns.label));
            }
        }

        for (ParsedInstruction parsedIns : parsedProgram.getInstructions()) {
            String targetLabel = null;
            boolean showError = false;

            switch (parsedIns.basicInstruction.type) {
                case InsConstants.INS_GOTO:
                    targetLabel = ((ParsedGoto) parsedIns.basicInstruction).targetLabel;
                    showError = !symbolTable.containsVariable(targetLabel);
                    break;
                case InsConstants.INS_IF:
                    targetLabel = ((ParsedIf) parsedIns.basicInstruction).gotoIns.targetLabel;
                    showError = !symbolTable.containsVariable(targetLabel);
                    break;
            }

            if (showError) {
                throw new RuntimeException("Error in instruction "
                        + parsedIns.getInsNumber() + " label: " + targetLabel
                        + " doesn't exist at beginning of a instruction.");
            }
        }

        return maxLabelsVarId;
    }
}
