/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax.program;

import edu.lengform.ssigma.semantic.SemanticHelper;
import edu.lengform.ssigma.semantic.instructions.ConditionalExpression;
import edu.lengform.ssigma.semantic.instructions.InsConstants;
import edu.lengform.ssigma.syntax.AvailableIdentifiers;
import edu.lengform.ssigma.syntax.SigmaPSymbols;

public class ParsedConditionalExpression {
    public static int maxNumsVarMemId = 0;
    public static int maxWordsVarMemId = 0;

    private final String leftVar;
    private final int intLeftVar;
    private final int op;
    private final char symbol;

    public ParsedConditionalExpression(String leftVarId, int op, char symbol) {
        super();
        this.leftVar = leftVarId;
        this.op = op;
        this.symbol = symbol;
        intLeftVar = SemanticHelper.GetMemID(leftVar);

        if (op == InsConstants.OP_NOT_CERO) {
            maxNumsVarMemId = Math.max(maxNumsVarMemId, intLeftVar);
        } else if (op == InsConstants.OP_BEGINS) {
            maxWordsVarMemId = Math.max(maxWordsVarMemId, intLeftVar);
        }
    }

    public ConditionalExpression buildProgramExpression() {
        return new ConditionalExpression(intLeftVar, op, symbol);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        switch (op) {
            case InsConstants.OP_NOT_CERO:
                sb.append(leftVar + SigmaPSymbols.OP_NO_EQ.getSymbol() + 0);
                break;
            case InsConstants.OP_BEGINS:
                sb.append(leftVar + " BEGINS " + symbol);
                break;
        }

        return sb.toString();
    }

    public ParsedConditionalExpression instantiate(AvailableIdentifiers ids) {
        String newLeftVar;
        if (op == InsConstants.OP_NOT_CERO) {
            newLeftVar = ids.getReplaceForNumVar(leftVar);
        } else {
            newLeftVar = ids.getReplaceForWordVar(leftVar);
        }
        return new ParsedConditionalExpression(newLeftVar, op, symbol);
    }
}
