/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax.program;

import edu.lengform.ssigma.semantic.SemanticHelper;
import edu.lengform.ssigma.syntax.AvailableIdentifiers;

public class ParsedInstruction {
    public final String label;
    public final int intlabel;
    public final ParsedBasicInstruction basicInstruction;
    private int insNumber;

    public ParsedInstruction(String label,
                             ParsedBasicInstruction basicInstruction) {
        super();
        this.label = label;
        this.basicInstruction = basicInstruction;
        if (label != null) {
            this.intlabel = SemanticHelper.GetMemID(label);
        } else {
            this.intlabel = 0;
        }
    }

    public int getInsNumber() {
        return insNumber;
    }

    public void setInsNumber(int insNumber) {
        this.insNumber = insNumber;
    }

    @Override
    public String toString() {
        return (label == null ? "" : label) + " " + basicInstruction.toString();
    }

    public ParsedInstruction instantiate(AvailableIdentifiers ids) {
        String newLabel = null;
        if (label != null) {
            newLabel = ids.getReplaceForLabel(label);
        }
        return new ParsedInstruction(newLabel,
                basicInstruction.instantiate(ids));
    }
}
