/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax.program;

import edu.lengform.ssigma.macro.Macro;
import edu.lengform.ssigma.macro.MacroDatabase;
import edu.lengform.ssigma.macro.MacroName;
import edu.lengform.ssigma.semantic.instructions.BasicInstruction;
import edu.lengform.ssigma.semantic.instructions.InsConstants;
import edu.lengform.ssigma.syntax.AvailableIdentifiers;

import java.util.ArrayList;

public class ParsedMacroInstance extends ParsedBasicInstruction {

    public final MacroName macroName;

    public ParsedMacroInstance(MacroName macroName) {
        super(InsConstants.INS_MACRO);
        this.macroName = macroName;
    }

    @Override
    public BasicInstruction buildProgramInstruction() {
        throw new RuntimeException(
                "Error: buildProgramInstruction: Macro is not an instruction.");
    }

    public void instantiate(MacroDatabase macrosDatabase,
                            ArrayList<ParsedInstruction> newInstructions,
                            AvailableIdentifiers ids) {

        final Macro macro = macrosDatabase.getMacro(macroName
                .getNameForDatabase());
        if (macro == null) {
            throw new RuntimeException(
                    "Error: Not exist a macro in database for instance: "
                            + macroName);
        }

        ids.clearRules();

        macroName.setReplacementRules(ids, macro.name);

        macro.instantiate(newInstructions, ids);
    }

    @Override
    public ParsedBasicInstruction instantiate(AvailableIdentifiers ids) {
        throw new RuntimeException(
                "Error: instantiate: Macro can't be instantiated.");
    }
}
