/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax.program;

import edu.lengform.ssigma.semantic.SemanticHelper;
import edu.lengform.ssigma.semantic.instructions.AlphabeticAssignment;
import edu.lengform.ssigma.semantic.instructions.BasicInstruction;
import edu.lengform.ssigma.semantic.instructions.InsConstants;
import edu.lengform.ssigma.syntax.AvailableIdentifiers;
import edu.lengform.ssigma.syntax.SigmaPSymbols;

public class ParsedAlphabeticAssignment extends ParsedBasicInstruction {
    public static int maxVarMemId = 0;

    private final String leftVar;
    private final int intLeftVar;
    private final String rightVar;
    private final int intRightVar;
    private final int op;
    private final char symbol;

    public ParsedAlphabeticAssignment(String leftVar, String rightVar, int op, char symbol) {
        super(InsConstants.INS_ALPHABETIC_ASSIGNMENT);
        this.leftVar = leftVar;
        this.rightVar = rightVar;
        this.op = op;
        this.symbol = symbol;
        intLeftVar = SemanticHelper.GetMemID(leftVar);
        if (rightVar != null) {
            intRightVar = SemanticHelper.GetMemID(rightVar);
        } else {
            intRightVar = 0;
        }

        maxVarMemId = Math.max(maxVarMemId, intLeftVar >= intRightVar ? intLeftVar : intRightVar);
    }

    @Override
    public BasicInstruction buildProgramInstruction() {
        return new AlphabeticAssignment(intLeftVar, op, intRightVar, symbol);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(leftVar + SigmaPSymbols.OP_ASIGN.getSymbol());

        switch (op) {
            case InsConstants.OP_ASSIGN_EPSILON:
                sb.append(SigmaPSymbols.EPSILON.getSymbol());
                break;
            case InsConstants.OP_CONCAT:
                sb.append(rightVar + " . '" + symbol + "'");
                break;
            case InsConstants.OP_RM_FIRST:
                sb.append(SigmaPSymbols.OP_DEL_FIRST_CHAR.getSymbol() + rightVar);
                break;
            case InsConstants.OP_ASSIGN_VAR:
                sb.append(rightVar);
                break;
        }

        return sb.toString();
    }

    @Override
    public ParsedBasicInstruction instantiate(AvailableIdentifiers ids) {
        final String newLeftVar = ids.getReplaceForWordVar(leftVar);

        String newRightVar = null;
        if (rightVar != null) {
            newRightVar = ids.getReplaceForWordVar(rightVar);
        }
        return new ParsedAlphabeticAssignment(newLeftVar, newRightVar, op, symbol);
    }
}
