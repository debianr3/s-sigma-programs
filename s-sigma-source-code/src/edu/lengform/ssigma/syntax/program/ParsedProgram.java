/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.syntax.program;

import java.util.ArrayList;

public class ParsedProgram {
    private final ArrayList<ParsedInstruction> instructions;
    private int maxNumsVarMemId;
    private int maxWordsVarMemId;

    public ParsedProgram() {
        ParsedNumericAssignment.maxVarMemId = 0;
        ParsedAlphabeticAssignment.maxVarMemId = 0;
        ParsedConditionalExpression.maxNumsVarMemId = 0;
        ParsedConditionalExpression.maxWordsVarMemId = 0;

        instructions = new ArrayList<ParsedInstruction>();
    }

    public void addInstruction(ParsedInstruction ins) {
        instructions.add(ins);
    }

    public ArrayList<ParsedInstruction> getInstructions() {
        return instructions;
    }

    public void onParseDone() {
        int i = 1;
        for (ParsedInstruction parsedIns : instructions) {
            parsedIns.setInsNumber(i++);
        }

        maxNumsVarMemId = Math.max(ParsedNumericAssignment.maxVarMemId,
                ParsedConditionalExpression.maxNumsVarMemId);
        maxWordsVarMemId = Math.max(ParsedAlphabeticAssignment.maxVarMemId,
                ParsedConditionalExpression.maxWordsVarMemId);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ParsedInstruction parsedInstruction : instructions) {
            sb.append(parsedInstruction);
            sb.append('\n');
        }

        return sb.toString();
    }

    public int getMaxNumsVarMemId() {
        return maxNumsVarMemId;
    }

    public int getMaxWordsVarMemId() {
        return maxWordsVarMemId;
    }

    public void replaceInstructions(ArrayList<ParsedInstruction> newInstructions) {
        instructions.clear();
        instructions.addAll(newInstructions);
        int i = 1;
        for (ParsedInstruction parsedIns : instructions) {
            parsedIns.setInsNumber(i++);
        }
    }
}
