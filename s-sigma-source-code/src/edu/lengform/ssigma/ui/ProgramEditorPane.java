/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import edu.lengform.ssigma.syntax.SigmaPSymbols;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class ProgramEditorPane extends JEditorPane implements KeyListener {
    private static final long serialVersionUID = 7879410643108128583L;

    private final JPopupMenu symbolsPopup;
    private final Zoom zoom;

    public ProgramEditorPane() {

        symbolsPopup = buildSymbolsPopup();
        addKeyListener(this);
        zoom = new Zoom();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0) {
            if ((e.getKeyCode() == KeyEvent.VK_SPACE)) {

                Rectangle rect = getBounds();
                try {
                    rect = modelToView(getCaretPosition());
                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }

                symbolsPopup.show(this, rect.x, rect.y);
            } else if ((e.getKeyCode() == KeyEvent.VK_M)) {

                Rectangle rect = getBounds();
                try {
                    rect = modelToView(getCaretPosition());
                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }
                buildMacroNamesPopup().show(this, rect.x, rect.y);
            } else if ((e.getKeyCode() == KeyEvent.VK_I)) {
                zoom.zoomIn();
            } else if ((e.getKeyCode() == KeyEvent.VK_O)) {
                zoom.zoomOut();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void paint(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

        super.paint(g2);
    }

    private JPopupMenu buildSymbolsPopup() {
        final JPopupMenu menu = new JPopupMenu();

        List<SigmaPSymbols> list = new ArrayList<SigmaPSymbols>();
        list.add(0, SigmaPSymbols.OP_ASIGN);
        list.add(1, SigmaPSymbols.OP_NO_EQ);
        list.add(2, null);
        list.add(3, SigmaPSymbols.OP_MINUS);
        list.add(4, SigmaPSymbols.OP_PLUS);
        list.add(5, null);
        list.add(6, SigmaPSymbols.OP_DEL_FIRST_CHAR);
        list.add(7, SigmaPSymbols.OP_CONCAT);
        list.add(8, SigmaPSymbols.EPSILON);

        for (SigmaPSymbols symbol : list) {
            if (symbol != null) {
                JMenuItem item = new JMenuItem("" + symbol.getSymbol());
                item.setFont(FontManager.INSTANCE.getFont(18));
                item.addActionListener(new ItemAction<SigmaPSymbols>(menu, symbol));
                menu.add(item);
            } else {
                menu.addSeparator();
            }
        }
        return menu;
    }

    private JPopupMenu buildMacroNamesPopup() {
        final JPopupMenu menu = new JPopupMenu();

        List<String> list = UIMacrosNamesList.INSTANCE.createMacroNamesList();

        for (String macroName : list) {
            JMenuItem item = new JMenuItem(macroName);
            item.setFont(FontManager.INSTANCE.getFont(12));
            item.addActionListener(new ItemAction<String>(menu, macroName));
            menu.add(item);
        }
        return menu;
    }

    private class ItemAction<T> extends AbstractAction {
        private static final long serialVersionUID = -5772576616277743101L;

        private final JPopupMenu menu;
        private final T objectData;

        public ItemAction(JPopupMenu menu, T objectData) {
            this.menu = menu;
            this.objectData = objectData;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Document doc = ProgramEditorPane.this.getDocument();
            try {
                doc.insertString(ProgramEditorPane.this.getCaretPosition(), "" + objectData.toString(), null);
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }

            menu.setVisible(false);
        }
    }

    private class Zoom {
        private static final int MAX_SIZES = 20;
        private final List<Font> sizes;
        private int currentSize = 2;

        public Zoom() {
            sizes = new ArrayList<Font>();
            for (int i = 0; i < MAX_SIZES; i++) {
                sizes.add(FontManager.INSTANCE.getFont(12 + i));
            }
            setCurrent();
        }

        public void zoomIn() {
            if (currentSize < MAX_SIZES - 1) {
                currentSize++;
                setCurrent();
            }
        }

        public void zoomOut() {
            if (currentSize > 0) {
                currentSize--;
                setCurrent();
            }
        }

        private void setCurrent() {
            ProgramEditorPane.this.setFont(sizes.get(currentSize));
        }
    }
}
