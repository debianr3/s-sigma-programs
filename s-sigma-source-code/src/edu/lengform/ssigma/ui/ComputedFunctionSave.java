/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import edu.lengform.ssigma.macro.MacrosLoader;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

public class ComputedFunctionSave extends JDialog {
    private static final long serialVersionUID = 6693693850749286867L;
    private final Action action = new SwingAction();
    private final String functionName;
    private JPanel contentPane;
    private JEditorPane editorPane;

    /**
     * Create the frame.
     */
    public ComputedFunctionSave(JFrame mainFrame, final String functionName, final String programString) {
        super(mainFrame);
        setModal(true);
        setModalityType(ModalityType.APPLICATION_MODAL);
        this.functionName = functionName;
        setTitle("Guardar macro");
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 550, 448);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);

        JScrollPane scrollPane = new JScrollPane();

        JButton btnSave = new JButton("Guardar");
        btnSave.setAction(action);
        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                gl_contentPane
                        .createSequentialGroup()
                        .addGroup(
                                gl_contentPane
                                        .createParallelGroup(Alignment.LEADING)
                                        .addGroup(
                                                gl_contentPane.createSequentialGroup().addGap(229)
                                                        .addComponent(btnSave))
                                        .addGroup(
                                                gl_contentPane
                                                        .createSequentialGroup()
                                                        .addContainerGap()
                                                        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 516,
                                                                GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(38, Short.MAX_VALUE)));
        gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
                Alignment.TRAILING,
                gl_contentPane.createSequentialGroup()
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE).addGap(18)
                        .addComponent(btnSave)));

        editorPane = new JEditorPane();
        scrollPane.setViewportView(editorPane);
        contentPane.setLayout(gl_contentPane);
        editorPane.setText(programString);
    }

    /**
     * Launch the application.
     */
    public static void openComputedFunctionView(final JFrame mainFrame, final String functionName, final String programString) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ComputedFunctionSave frame = new ComputedFunctionSave(mainFrame, functionName, programString);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected JEditorPane getEditorPane() {
        return editorPane;
    }

    private class SwingAction extends AbstractAction {
        private static final long serialVersionUID = 6034828509309240474L;

        public SwingAction() {
            putValue(NAME, "Guardar macro");
            putValue(SHORT_DESCRIPTION, "Guardar macro.");
        }

        public void actionPerformed(ActionEvent e) {
            final String macroText = getEditorPane().getText();

            final String fileName = functionName + ".sm";

            File f = new File(MacrosLoader.MACROS_DIR.getAbsolutePath() + File.separator + fileName);
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
                writer.write(macroText);
                JOptionPane.showMessageDialog(ComputedFunctionSave.this, "Macro guardado correctamente.");
                dispose();
            } catch (IOException ex) {
            } finally {
                try {
                    if (writer != null)
                        writer.close();
                } catch (IOException ex) {
                }
            }
        }
    }
}
