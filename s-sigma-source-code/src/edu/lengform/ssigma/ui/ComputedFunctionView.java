/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import edu.lengform.ssigma.macro.MacroCreator;
import edu.lengform.ssigma.macro.MacroCreator.FunctionImage;
import edu.lengform.ssigma.syntax.SigmaPSymbols;
import util.Pair;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.concurrent.atomic.AtomicBoolean;

public class ComputedFunctionView extends JDialog {

    private static final long serialVersionUID = 2112861912250298075L;
    private final Action action;
    private final String textProgram;
    private final PseudoProgramGenerator pseudoProgramGenerator;
    private final JFrame mainFrame;
    private JPanel contentPane;
    private JTextField textFieldFunctionName;
    private JSpinner spinnerOmegaArgs;
    private JSpinner spinnerSigmaArgs;
    private JComboBox comboBoxImage;

    /**
     * Create the frame.
     */
    public ComputedFunctionView(JFrame mainFrame, final String textProgram, PseudoProgramGenerator pseudoProgramGenerator) {
        super(mainFrame);
        this.mainFrame = mainFrame;
        setModal(true);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setTitle("Compute a function");
        this.textProgram = textProgram;
        this.pseudoProgramGenerator = pseudoProgramGenerator;
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 390, 254);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        action = new SwingAction(mainFrame);

        JLabel lblComputeAFunction = new JLabel("Guardar como macro");
        lblComputeAFunction.setFont(FontManager.INSTANCE.getFont(Font.BOLD, 14));

        JLabel lblFunctionName = new JLabel("Nombre del Macro:");

        JLabel lblNumeriircArguments = new JLabel("Argumentos:");

        JLabel lblImage = new JLabel("Tipo de retorno:");

        comboBoxImage = new JComboBox();
        comboBoxImage.setModel(new DefaultComboBoxModel(new String[]{SigmaPSymbols.OMEGA.toString(),
                SigmaPSymbols.SIGMA.toString() + "*"}));

        comboBoxImage.setFont(FontManager.INSTANCE.getFont(Font.BOLD, 14));

        JLabel lblua = new JLabel(SigmaPSymbols.OMEGA.toString());
        lblua.setFont(FontManager.INSTANCE.getFont(Font.BOLD, 20));

        spinnerOmegaArgs = new JSpinner();
        spinnerOmegaArgs.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));

        JLabel lblX = new JLabel("x");
        lblX.setFont(FontManager.INSTANCE.getFont(Font.BOLD, 16));

        JLabel lblNewLabel = new JLabel(SigmaPSymbols.SIGMA.toString() + "*");
        lblNewLabel.setFont(FontManager.INSTANCE.getFont(Font.BOLD, 20));

        spinnerSigmaArgs = new JSpinner();
        spinnerSigmaArgs.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));

        textFieldFunctionName = new JTextField();
        textFieldFunctionName.setColumns(10);

        JButton btnSave = new JButton("Guardar");
        btnSave.setAction(action);
        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane
                .setHorizontalGroup(gl_contentPane
                        .createParallelGroup(Alignment.TRAILING)
                        .addGroup(
                                gl_contentPane
                                        .createSequentialGroup()
                                        .addContainerGap(12, Short.MAX_VALUE)
                                        .addGroup(
                                                gl_contentPane
                                                        .createParallelGroup(Alignment.LEADING)
                                                        .addGroup(
                                                                gl_contentPane.createSequentialGroup().addGap(135)
                                                                        .addComponent(lblComputeAFunction))
                                                        .addGroup(
                                                                gl_contentPane
                                                                        .createSequentialGroup()
                                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                gl_contentPane
                                                                                        .createParallelGroup(
                                                                                                Alignment.LEADING)
                                                                                        .addComponent(lblFunctionName)
                                                                                        .addComponent(lblImage))
                                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                gl_contentPane
                                                                                        .createParallelGroup(
                                                                                                Alignment.LEADING)
                                                                                        .addComponent(
                                                                                                comboBoxImage,
                                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                                78,
                                                                                                GroupLayout.PREFERRED_SIZE)
                                                                                        .addGroup(
                                                                                                gl_contentPane
                                                                                                        .createParallelGroup(
                                                                                                                Alignment.LEADING)
                                                                                                        .addComponent(
                                                                                                                textFieldFunctionName,
                                                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                                                221,
                                                                                                                GroupLayout.PREFERRED_SIZE)
                                                                                                        .addGroup(
                                                                                                                gl_contentPane
                                                                                                                        .createSequentialGroup()
                                                                                                                        .addGroup(
                                                                                                                                gl_contentPane
                                                                                                                                        .createParallelGroup(
                                                                                                                                                Alignment.LEADING)
                                                                                                                                        .addGroup(
                                                                                                                                                gl_contentPane
                                                                                                                                                        .createSequentialGroup()
                                                                                                                                                        .addComponent(
                                                                                                                                                                lblua)
                                                                                                                                                        .addGap(66)
                                                                                                                                                        .addComponent(
                                                                                                                                                                lblX))
                                                                                                                                        .addGroup(
                                                                                                                                                gl_contentPane
                                                                                                                                                        .createSequentialGroup()
                                                                                                                                                        .addGap(12)
                                                                                                                                                        .addComponent(
                                                                                                                                                                spinnerOmegaArgs,
                                                                                                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                55,
                                                                                                                                                                GroupLayout.PREFERRED_SIZE))
                                                                                                                                        .addComponent(
                                                                                                                                                btnSave,
                                                                                                                                                Alignment.TRAILING))
                                                                                                                        .addGroup(
                                                                                                                                gl_contentPane
                                                                                                                                        .createParallelGroup(
                                                                                                                                                Alignment.LEADING)
                                                                                                                                        .addGroup(
                                                                                                                                                gl_contentPane
                                                                                                                                                        .createSequentialGroup()
                                                                                                                                                        .addGap(30)
                                                                                                                                                        .addComponent(
                                                                                                                                                                lblNewLabel))
                                                                                                                                        .addGroup(
                                                                                                                                                gl_contentPane
                                                                                                                                                        .createSequentialGroup()
                                                                                                                                                        .addGap(39)
                                                                                                                                                        .addComponent(
                                                                                                                                                                spinnerSigmaArgs,
                                                                                                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                55,
                                                                                                                                                                GroupLayout.PREFERRED_SIZE)))))))
                                                        .addGroup(
                                                                gl_contentPane.createSequentialGroup()
                                                                        .addPreferredGap(ComponentPlacement.RELATED)
                                                                        .addComponent(lblNumeriircArguments)))
                                        .addGap(189)));
        gl_contentPane
                .setVerticalGroup(gl_contentPane
                        .createParallelGroup(Alignment.LEADING)
                        .addGroup(
                                gl_contentPane
                                        .createSequentialGroup()
                                        .addComponent(lblComputeAFunction)
                                        .addGap(18)
                                        .addGroup(
                                                gl_contentPane
                                                        .createParallelGroup(Alignment.BASELINE)
                                                        .addComponent(lblFunctionName)
                                                        .addComponent(textFieldFunctionName,
                                                                GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(ComponentPlacement.UNRELATED)
                                        .addGroup(
                                                gl_contentPane
                                                        .createParallelGroup(Alignment.TRAILING)
                                                        .addGroup(
                                                                gl_contentPane.createParallelGroup(Alignment.BASELINE)
                                                                        .addComponent(lblNumeriircArguments)
                                                                        .addComponent(lblua).addComponent(lblX)
                                                                        .addComponent(lblNewLabel))
                                                        .addGroup(
                                                                gl_contentPane
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                gl_contentPane
                                                                                        .createParallelGroup(
                                                                                                Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                spinnerOmegaArgs,
                                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(
                                                                                                spinnerSigmaArgs,
                                                                                                GroupLayout.PREFERRED_SIZE,
                                                                                                GroupLayout.DEFAULT_SIZE,
                                                                                                GroupLayout.PREFERRED_SIZE))
                                                                        .addGap(26)))
                                        .addGap(18)
                                        .addGroup(
                                                gl_contentPane
                                                        .createParallelGroup(Alignment.BASELINE)
                                                        .addComponent(lblImage)
                                                        .addComponent(comboBoxImage, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                                        .addComponent(btnSave)));
        contentPane.setLayout(gl_contentPane);
    }

    /**
     * Launch the application.
     */
    public static void openComputeAfunctionView(final JFrame parent,
                                                final String pseudoProgram,
                                                final PseudoProgramGenerator pseudoProgramGenerator) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ComputedFunctionView frame = new ComputedFunctionView(parent, pseudoProgram, pseudoProgramGenerator);
                    frame.pack();
                    frame.setVisible(true);
                    frame.setLocationRelativeTo(parent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected JSpinner getSpinnerOmegaArgs() {
        return spinnerOmegaArgs;
    }

    protected JSpinner getSpinnerSigmaArgs() {
        return spinnerSigmaArgs;
    }

    protected JComboBox getComboBoxImage() {
        return comboBoxImage;
    }

    public interface PseudoProgramGenerator {
        public Pair<String, Integer> generatePseudoProgram(String textProgram, int omegaArgsCount, int sigmaArgsCount);
    }

    private class SwingAction extends AbstractAction {
        private static final long serialVersionUID = -6190585564024810988L;
        private final AtomicBoolean processing = new AtomicBoolean();
        private final JFrame mainFrame;

        public SwingAction(JFrame mainFrame) {
            this.mainFrame = mainFrame;
            putValue(NAME, "Guardar");
            putValue(SHORT_DESCRIPTION, "Guardar macro.");
        }

        public void actionPerformed(ActionEvent e) {
            final String functionName = textFieldFunctionName.getText();
            final int omegaArgs = (Integer) getSpinnerOmegaArgs().getValue();
            final int sigmaArgs = (Integer) getSpinnerSigmaArgs().getValue();
            final boolean isOmegaImage = SigmaPSymbols.OMEGA.toString().equals(
                    (String) getComboBoxImage().getSelectedItem());

            if (processing.get()) {
                return;
            }

            processing.set(true);

            try {
                Pair<String, Integer> pair = pseudoProgramGenerator.generatePseudoProgram(textProgram, omegaArgs,
                        sigmaArgs);

                final String pseudoProgram = pair.getLeft();
                final int maxId = pair.getRight();

                MacroCreator.Builder builder = new MacroCreator.Builder(functionName,
                        isOmegaImage ? FunctionImage.OMEGA : FunctionImage.SIGMA, pseudoProgram).omegaArgs(omegaArgs)
                        .sigmaArgs(sigmaArgs).maxId(maxId);

                ComputedFunctionSave.openComputedFunctionView(mainFrame, functionName, builder.buildMacro());
            } finally {
                processing.set(false);
                dispose();
            }
        }
    }
}
