/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import edu.lengform.ssigma.macro.MacrosLoader;
import edu.lengform.ssigma.syntax.SigmaPSymbols;

import java.util.*;

public enum UIMacrosNamesList {

    INSTANCE;

    private final Set<String> macroNames = Collections.synchronizedSet(new TreeSet<String>());

    private List<String> listOfMacros;

    public void reloadAllMacros() {
        macroNames.clear();
        macroNames.addAll(new MacrosLoader(MacrosLoader.MACROS_DIR).loadMacros().getFullMacroNames());

        synchronized (this) {
            ArrayList<String> macroNamesList = new ArrayList<>(macroNames);
            Collections.sort(macroNamesList, new MacroNamesComparator());
            listOfMacros = Collections.unmodifiableList(macroNamesList);
        }
    }

    public List<String> createMacroNamesList() {
        synchronized (this) {
            return new ArrayList<>(listOfMacros);
        }
    }

    private static class MacroNamesComparator implements Comparator<String> {

        @Override
        public int compare(String name1, String name2) {
            final int start1 = name1.indexOf(SigmaPSymbols.OP_ASIGN.toString());
            final int start2 = name2.indexOf(SigmaPSymbols.OP_ASIGN.toString());

            if (start1 >= 0 && start2 >= 0) {
                return name1.substring(start1).compareToIgnoreCase(name2.substring(start2));
            }
            return name1.compareToIgnoreCase(name2);
        }
    }
}
