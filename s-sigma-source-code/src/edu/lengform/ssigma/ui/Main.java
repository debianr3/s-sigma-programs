/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.ui;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;

public class Main extends JFrame {

    public static void main(String[] args) {
        setEncoding();
        FontManager.INSTANCE.setUIFont();
        initializeThreads();

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Main frame = new Main();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void setEncoding() {
        try {
            System.setProperty("file.encoding", "UTF-8");
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (Throwable e) {
            // Ignore.
        }
    }

    private static void initializeThreads() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                UIMacrosNamesList.INSTANCE.reloadAllMacros();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    UIMacrosNamesList.INSTANCE.reloadAllMacros();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }
        }, "Macros Updater Thread").start();
    }

    public Main() {
        UIController.INSTANCE.setMainFrame(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 900, 700);
        setMinimumSize(new Dimension(900, 700));

        Container contentPane = getContentPane();

        BoxLayout mainLayout = new BoxLayout(contentPane, BoxLayout.Y_AXIS);
        contentPane.setLayout(mainLayout);

        contentPane.add(Box.createRigidArea(new Dimension(0, 5)));
        contentPane.add(buildButtonsLayout());
        contentPane.add(Box.createRigidArea(new Dimension(0, 5)));
        contentPane.add(buildInitialStateLayout());
        contentPane.add(buildProgramLayout());
        contentPane.add(buildMessagesConsole());
    }

    private JComponent buildButtonsLayout() {
        final JPanel panel = new JPanel();
        {
            BoxLayout mainLayout = new BoxLayout(panel, BoxLayout.X_AXIS);
            panel.setLayout(mainLayout);
        }

        {// Label
            panel.add(Box.createRigidArea(new Dimension(4, 0)));
            final JLabel steepsLabel = new JLabel("Pasos a ejecutar:");
            steepsLabel.setHorizontalAlignment(JLabel.LEFT);
            panel.add(steepsLabel);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }

        {// Spinner
            final JSpinner stepsSpinner = new JSpinner();
            final Dimension size = new Dimension(100, 40);
            stepsSpinner.setMaximumSize(size);
            stepsSpinner.setMinimumSize(size);
            stepsSpinner.setPreferredSize(size);
            UIController.INSTANCE.setStepsSpinner(stepsSpinner);
            panel.add(stepsSpinner);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }


        {// Run btn.
            final JButton btn = new JButton("Ejecutar");
            UIController.INSTANCE.setBtnRun(btn);
            panel.add(btn);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }

        {// Stop btn.
            final JButton btn = new JButton("Detener");
            UIController.INSTANCE.setBtnStop(btn);
            panel.add(btn);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }
        panel.add(Box.createHorizontalGlue());

        {// Stop btn.
            final JButton btn = new JButton("Guardar como macro");
            UIController.INSTANCE.setBtnSaveAsMacro(btn);
            panel.add(btn);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }

        {// View program btn.
            final JButton btn = new JButton("Ver el programa");
            UIController.INSTANCE.setBtnViewProgram(btn);
            panel.add(btn);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }

        {// Help btn.
            final JButton btn = new JButton("Ayuda");
            UIController.INSTANCE.setBtnHelp(btn);
            panel.add(btn);
            panel.add(Box.createRigidArea(new Dimension(10, 0)));
        }
        return panel;
    }

    private JComponent buildInitialStateLayout() {
        final JPanel panel = new JPanel();
        {
            {// Size
                panel.setPreferredSize(new Dimension(Integer.MAX_VALUE, 120));
            }

            BoxLayout layout = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
            panel.setLayout(layout);
            panel.setBorder(new TitledBorder("Estado inicial"));

            final JScrollPane scrollPane = new JScrollPane();
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

            {// Editor
                final JEditorPane initialStateEditor = new JEditorPane();
                initialStateEditor.setToolTipText("Los datos de entrada de la forma: N1 = 10, P3 = abc");
                UIController.INSTANCE.setInitialStateEditor(initialStateEditor);
                scrollPane.setViewportView(initialStateEditor);
            }

            panel.add(scrollPane);
        }

        return panel;
    }

    private JComponent buildProgramLayout() {
        final JPanel panel = new JPanel();
        {
            BoxLayout layout = new BoxLayout(panel, BoxLayout.X_AXIS);
            panel.setLayout(layout);
        }
        {// Program editor
            JPanel programPanel = new JPanel();
            BoxLayout layout = new BoxLayout(programPanel, BoxLayout.X_AXIS);
            programPanel.setLayout(layout);
            programPanel.setBorder(new TitledBorder("Programa"));


            ProgramEditorPane programEditor = new ProgramEditorPane();
            UIController.INSTANCE.setProgramEditor(programEditor);

            programPanel.add(new JScrollPane(programEditor));

            panel.add(programPanel);
        }
        {// Program state
            JPanel programState = new JPanel();
            {
                BoxLayout layout = new BoxLayout(programState, BoxLayout.X_AXIS);
                programState.setLayout(layout);
            }
            programState.setBorder(new TitledBorder("Estado del programa"));

            // Alphabetic variables.
            programState.add(buildProgramStateVariablesList(true));

            // Alphabetic variables.
            programState.add(buildProgramStateVariablesList(false));


            panel.add(programState);
        }

        return panel;
    }

    private JComponent buildProgramStateVariablesList(boolean isNumericList) {
        JPanel panel = new JPanel();
        {
            BoxLayout layout = new BoxLayout(panel, BoxLayout.X_AXIS);
            panel.setLayout(layout);
        }

        String title = "Variables Numericas";
        if (!isNumericList) {
            title = "Variables Alfabeticas";
        }

        panel.setBorder(new TitledBorder(title));

        final JScrollPane scrollPane = new JScrollPane();
        {
            final JList list = new JList();
            if (isNumericList) {
                UIController.INSTANCE.setNumVarsList(list);
            } else {
                UIController.INSTANCE.setAlphabeticVarsList(list);
            }
            scrollPane.setViewportView(list);
            Dimension preferredSize = scrollPane.getPreferredSize();
            preferredSize = new Dimension(200, preferredSize.height);
            scrollPane.setMaximumSize(new Dimension(215, Integer.MAX_VALUE));
            scrollPane.setPreferredSize(preferredSize);
        }
        panel.add(scrollPane);

        return panel;
    }

    private JComponent buildMessagesConsole() {
        final JPanel panel = new JPanel();
        {
            panel.setPreferredSize(new Dimension(Integer.MAX_VALUE, 200));

            {// Layout
                BoxLayout layout = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
                panel.setLayout(layout);
                panel.setBorder(new TitledBorder("Mensajes y Errores"));
            }

            final JScrollPane scrollPane = new JScrollPane();
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

            {// Editor
                final JTextPane consoleTextPane = new JTextPane();
                scrollPane.setViewportView(consoleTextPane);
                UIController.INSTANCE.setMessageConsole(consoleTextPane);
            }

            panel.add(scrollPane);
        }

        return panel;
    }
}
