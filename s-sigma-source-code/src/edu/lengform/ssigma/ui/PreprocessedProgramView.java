/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import java.awt.*;

public class PreprocessedProgramView {

    private JDialog frmPreprocessedProgram;
    private JEditorPane editorPane;

    /**
     * Create the application.
     */
    private PreprocessedProgramView(final JFrame parent, final String programString) {
        initialize(parent);
        getEditorPane().setText(programString);
        frmPreprocessedProgram.setVisible(true);
    }

    /**
     * Launch the application.
     */
    public static void openProcessedProgramView(final JFrame parent, final String programString) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new PreprocessedProgramView(parent, programString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize(final JFrame parent) {
        frmPreprocessedProgram = new JDialog(parent);
        frmPreprocessedProgram.setModal(true);
        frmPreprocessedProgram.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        frmPreprocessedProgram.setTitle("Programa completo");
        frmPreprocessedProgram.setBounds(100, 100, 450, 596);
        frmPreprocessedProgram.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel panel = new JPanel();
        frmPreprocessedProgram.getContentPane().add(panel, BorderLayout.NORTH);
        GroupLayout gl_panel = new GroupLayout(panel);
        gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(
                Alignment.LEADING).addGap(0, 442, Short.MAX_VALUE));
        gl_panel.setVerticalGroup(gl_panel.createParallelGroup(
                Alignment.LEADING).addGap(0, 69, Short.MAX_VALUE));
        panel.setLayout(gl_panel);

        JScrollPane scrollPane = new JScrollPane();
        frmPreprocessedProgram.getContentPane().add(scrollPane, BorderLayout.CENTER);

        editorPane = new JEditorPane();
        editorPane.setEditable(false);
        scrollPane.setViewportView(editorPane);
        frmPreprocessedProgram.setLocationRelativeTo(parent);
    }

    protected JEditorPane getEditorPane() {
        return editorPane;
    }
}
