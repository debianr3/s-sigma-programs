/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;

public enum FontManager {
    INSTANCE;

    private Font font;

    FontManager() {
        font = new Font("DejaVuSans", Font.PLAIN, 12);
    }

    public void setUIFont() {
        final FontUIResource fontUIResource = new FontUIResource(font);

        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value != null && value instanceof javax.swing.plaf.FontUIResource)
                UIManager.put(key, fontUIResource);
        }
    }

    public Font getFont(float size) {
        return font.deriveFont(size);
    }

    public Font getFont(int style, float size) {
        return font.deriveFont(style, size);
    }
}
