/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.ui;

import edu.lengform.ssigma.macro.MacrosLoader;
import edu.lengform.ssigma.runtime.ProgramCallbacks;
import edu.lengform.ssigma.runtime.ProgramExecutor;
import edu.lengform.ssigma.semantic.ProgramInput;
import edu.lengform.ssigma.semantic.instructions.Program;
import util.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.StringReader;
import java.util.concurrent.atomic.AtomicBoolean;

public enum UIController {
    INSTANCE;

    private final ProgramExecutor programExecutor = new ProgramExecutor();

    private NumVarsListModel numVarsListModel;
    private AlphabeticsVarsListModel alphabeticsVarsListModel;
    private JList numVarsList;
    private JList alphabeticVarsList;
    private MessageConsole messageConsole;
    private JButton btnRun;
    private JButton btnStop;
    private JButton btnSaveAsMacro;
    private JButton btnViewProgram;
    private JButton btnHelp;
    private JEditorPane programEditor;
    private JEditorPane initialStateEditor;
    private JSpinner stepsSpinner;
    private JFrame mainFrame;

    public void setNumVarsList(JList numVarsList) {
        this.numVarsList = numVarsList;
        numVarsListModel = new NumVarsListModel();
        numVarsList.setModel(numVarsListModel);
    }

    public void setAlphabeticVarsList(JList alphabeticVarsList) {
        this.alphabeticVarsList = alphabeticVarsList;
        alphabeticsVarsListModel = new AlphabeticsVarsListModel();
        alphabeticVarsList.setModel(alphabeticsVarsListModel);
    }

    public void setMessageConsole(JTextPane outputTextPane) {
        this.messageConsole = new MessageConsole(outputTextPane);
        messageConsole.redirectErr(Color.RED, null);
        messageConsole.setMessageLines(100);
    }

    public void setBtnRun(JButton btnRun) {
        this.btnRun = btnRun;
        btnRun.setBackground(Color.GREEN);
        btnRun.setAction(new RunAction());
    }

    public void setBtnStop(JButton btnStop) {
        this.btnStop = btnStop;
        btnStop.setAction(new StopAction());
        btnStop.setEnabled(false);
    }

    public void setBtnSaveAsMacro(JButton btnSaveAsMacro) {
        this.btnSaveAsMacro = btnSaveAsMacro;
        btnSaveAsMacro.setAction(new SaveAsMacroAction());
    }

    public void setBtnViewProgram(JButton btnViewProgram) {
        this.btnViewProgram = btnViewProgram;
        btnViewProgram.setAction(new ViewProgramAction());
    }

    public void setBtnHelp(JButton btnHelp) {
        this.btnHelp = btnHelp;
        btnHelp.setAction(new HelpAction());
    }

    public void setProgramEditor(JEditorPane programEditor) {
        this.programEditor = programEditor;
    }

    public void setInitialStateEditor(JEditorPane initialStateEditor) {
        this.initialStateEditor = initialStateEditor;
    }

    public void setStepsSpinner(JSpinner stepsSpinner) {
        this.stepsSpinner = stepsSpinner;
        stepsSpinner.setModel(new SpinnerNumberModel(new Integer(-1),
                new Integer(-1),
                null,
                new Integer(1)));
        stepsSpinner.setEditor(new StepsToRunSpinner(stepsSpinner));
    }

    public void setMainFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    private String getInitialStateAsString() {
        String initialState = "";
        if (initialStateEditor.getText() != null) {
            initialState = initialStateEditor.getText().trim();
        }
        return initialState;
    }

    private class ProgramCallbacksImpl implements ProgramCallbacks {

        @Override
        public void onProgramEnds(Program program) {
            btnRun.setBackground(Color.GREEN);
            btnStop.setEnabled(false);

            numVarsListModel.setMemRef(program.state.exposeNumMem());
            alphabeticsVarsListModel.setMemRef(program.state.exposeWordsMem());
            numVarsList.invalidate();
            alphabeticVarsList.invalidate();
        }

        @Override
        public void onProgramStart(Program program) {
            btnRun.setBackground(Color.RED);
            btnStop.setEnabled(true);
        }
    }

    private class ProgramExceptionHandler implements Thread.UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            e.printStackTrace();
            programExecutor.interrupt();
            programExecutor.hackRunning();
            btnRun.setBackground(Color.GREEN);
            btnStop.setEnabled(false);
        }
    }

    private class RunAction extends AbstractAction {
        private static final long serialVersionUID = 8462692000396538324L;

        public RunAction() {
            putValue(NAME, "Ejecutar");
            putValue(SHORT_DESCRIPTION, "Ejecutar el programa.");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            messageConsole.clear();
            if (!programExecutor.isRunning()) {
                final String text = programEditor.getText();
                if (text.trim().isEmpty()) {
                    return;
                }
                final StringReader reader = new StringReader(text);
                final int steps = (Integer) stepsSpinner.getValue();

                try {
                    programExecutor.run(steps, reader, new ProgramCallbacksImpl(),
                            new ProgramExceptionHandler(), new ProgramInput(getInitialStateAsString()),
                            MacrosLoader.MACROS_DIR);
                } catch (RuntimeException e1) {
                }
            }
        }
    }

    private class StopAction extends AbstractAction {
        private static final long serialVersionUID = 7250315486196269997L;

        public StopAction() {
            putValue(NAME, "Detener");
            putValue(SHORT_DESCRIPTION, "Interrumpe la ejecucion del programa.");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                programExecutor.interrupt();
            } catch (Exception ex) {
            }
        }
    }

    private class HelpAction extends AbstractAction {
        private static final long serialVersionUID = -7453269490208752674L;

        public HelpAction() {
            putValue(NAME, "?");
            putValue(SHORT_DESCRIPTION, "Ayuda");
        }

        public void actionPerformed(ActionEvent e) {
            JDialog dialog = new JDialog(mainFrame);
            dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            dialog.setTitle("Acerca de");
            dialog.getContentPane().add(new About());
            dialog.pack();
            dialog.setSize(700, 400);
            dialog.setVisible(true);
            dialog.setLocationRelativeTo(mainFrame);
        }
    }

    private class ViewProgramAction extends AbstractAction {
        private static final long serialVersionUID = -42878100843846271L;
        private final AtomicBoolean processing = new AtomicBoolean();

        public ViewProgramAction() {
            putValue(NAME, "Ver el programa");
            putValue(SHORT_DESCRIPTION, "Ver el programa completo luego de instanciar macros, etc.");
        }

        public void actionPerformed(ActionEvent e) {
            final String text = programEditor.getText();
            if (text.trim().isEmpty() || processing.get()) {
                return;
            }

            processing.set(true);

            try {
                final StringReader reader = new StringReader(text);

                PreprocessedProgramView.openProcessedProgramView(mainFrame, new ProgramExecutor().getPreprocessedProgram(reader,
                        new ProgramInput(getInitialStateAsString()), MacrosLoader.MACROS_DIR));
            } finally {
                processing.set(false);
            }
        }
    }

    private class SaveAsMacroAction extends AbstractAction {
        private static final long serialVersionUID = 319543094642199400L;
        private final AtomicBoolean processing = new AtomicBoolean();

        public SaveAsMacroAction() {
            putValue(NAME, "Guardar como macro");
            putValue(SHORT_DESCRIPTION, "Guarda el programa actual como un macro.");
        }

        public void actionPerformed(ActionEvent e) {
            final String text = programEditor.getText();
            if (text.trim().isEmpty() || processing.get()) {
                return;
            }

            processing.set(true);

            final ComputedFunctionView.PseudoProgramGenerator pseudoProgramGenerator = new ComputedFunctionView.PseudoProgramGenerator() {
                @Override
                public Pair<String, Integer> generatePseudoProgram(String textProgram, int omegaArgsCount, int sigmaArgsCount) {
                    return new ProgramExecutor().getPreprocessedPseudoProgram(new StringReader(textProgram),
                            new ProgramInput(getInitialStateAsString()),
                            MacrosLoader.MACROS_DIR,
                            omegaArgsCount,
                            sigmaArgsCount);
                }
            };

            try {
                ComputedFunctionView.openComputeAfunctionView(mainFrame, text, pseudoProgramGenerator);
            } finally {
                processing.set(false);
            }
        }
    }
}
