/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import java.io.*;

public class OutputController {

    private static final String FILE = "ProgramTrace.txt";

    public static void setOutputToFile() {
        try {
            PrintStream printStream = new PrintStream(new File(FILE));

            System.setOut(printStream);

        } catch (FileNotFoundException e) {
        }
    }

    public static void setOutputToDefault() {
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
    }
}
