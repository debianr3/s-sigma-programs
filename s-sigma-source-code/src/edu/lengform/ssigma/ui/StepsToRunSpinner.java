/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.ui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class StepsToRunSpinner extends JPanel implements ChangeListener {
    private static final long serialVersionUID = -3994916816686212972L;

    private JLabel label = new JLabel();

    public StepsToRunSpinner(JSpinner spinner) {
        setLayout(new FlowLayout(FlowLayout.LEADING));
        label.setForeground(Color.BLACK);
        setBackground(Color.WHITE);
        setBorder(new EmptyBorder(5, 6, 5, 0));
        add(label);
        spinner.addChangeListener(this);
        label.setText("Sin limite");
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSpinner spinner = (JSpinner) e.getSource();
        int value = (Integer) spinner.getValue();
        if (value >= 0) {
            label.setText(spinner.getValue().toString());
        } else {
            label.setText("Sin limite");
        }
    }
}