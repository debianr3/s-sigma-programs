/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.macro;

import edu.lengform.ssigma.syntax.SigmaPSymbols;

public class MacroCreator {
    public enum FunctionImage {
        OMEGA, SIGMA
    }

    public static class Builder {
        private final String functionName;
        private final FunctionImage image;
        private final String pseudoProgram;
        private int omegaArgs = 0;
        private int sigmaArgs = 0;
        private int maxId = 0;

        public Builder(String functionName, FunctionImage image, String pseudoProgram) {
            this.functionName = functionName;
            this.image = image;
            this.pseudoProgram = pseudoProgram;
        }

        public Builder omegaArgs(int count) {
            omegaArgs = count;
            return this;
        }

        public Builder sigmaArgs(int count) {
            sigmaArgs = count;
            return this;
        }

        public Builder maxId(int id) {
            maxId = id;
            return this;
        }

        private String buildName() {
            StringBuilder sb = new StringBuilder("[");

            switch (image) {
                case OMEGA:
                    sb.append("V" + (omegaArgs + 1));
                    break;
                case SIGMA:
                    sb.append("W" + (sigmaArgs + 1));
                    break;
                default:
                    throw new IllegalStateException("Illegal function image: " + image);
            }

            sb.append(SigmaPSymbols.OP_ASIGN.getSymbol() + functionName);

            sb.append('(');
            for (int w = 1; w <= omegaArgs; w++) {
                sb.append("V" + w);
                sb.append(',');
            }
            if (omegaArgs > 0 && sigmaArgs == 0) {
                sb.deleteCharAt(sb.length() - 1);
            }

            for (int s = 1; s <= sigmaArgs; s++) {
                sb.append("W" + s);
                sb.append(',');
            }
            if (sigmaArgs > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            sb.append(")]");

            return sb.toString();
        }

        private String buildMacroBody() {

            StringBuilder sb = new StringBuilder();

            final int k = maxId;

            // Enable for debug.
            // sb.append("// n = " + omegaArgs + "\n");
            // sb.append("// m = " + sigmaArgs + "\n");
            // sb.append("// K = " + k + "\n\n\n");

            if (omegaArgs > 0) {
                final String ASSIGN = "" + SigmaPSymbols.OP_ASIGN.getSymbol();
                for (int n = 1; n <= k; n++) {
                    if (n <= omegaArgs) {
                        sb.append("V" + (omegaArgs + n) + ASSIGN + "V" + n);
                    } else {
                        sb.append("V" + (omegaArgs + n) + ASSIGN + "0");
                    }
                    sb.append('\n');
                }
            }

            if (sigmaArgs > 0) {
                final String ASSIGN = "" + SigmaPSymbols.OP_ASIGN.getSymbol();
                final char EPSILON = SigmaPSymbols.EPSILON.getSymbol();
                for (int m = 1; m <= k; m++) {
                    if (m <= sigmaArgs) {
                        sb.append("W" + (sigmaArgs + m) + ASSIGN + "W" + m);
                    } else {
                        sb.append("W" + (sigmaArgs + m) + ASSIGN + EPSILON);
                    }
                    sb.append('\n');
                }
            }
            sb.append(pseudoProgram);

            return sb.toString();
        }

        public String buildMacro() {
            StringBuilder sb = new StringBuilder("#BEGIN MACRO\n\n");
            sb.append("#NAME: " + buildName());
            sb.append('\n');
            sb.append('\n');

            sb.append(buildMacroBody());

            sb.append('\n');

            sb.append("#END MACRO");

            return sb.toString();
        }
    }
}
