/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.lengform.ssigma.macro;

import edu.lengform.ssigma.syntax.AvailableIdentifiers;
import edu.lengform.ssigma.syntax.program.ParsedInstruction;
import edu.lengform.ssigma.syntax.program.ParsedProgram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Macro {

    public final MacroName name;
    private final ParsedProgram pseudoProgram;
    private final HashMap<String, String> hashIds;
    private final ArrayList<String> identifiers;

    public Macro(MacroName name, ParsedProgram pseudoProgram) {
        this.name = name;
        this.pseudoProgram = pseudoProgram;
        identifiers = getIdentifiers();
        hashIds = new HashMap<String, String>();
        for (String id : identifiers) {
            hashIds.put(id, id);
        }

    }

    public void instantiate(ArrayList<ParsedInstruction> newInstructions, AvailableIdentifiers ids) {
        for (ParsedInstruction parsedIns : pseudoProgram.getInstructions()) {
            newInstructions.add(parsedIns.instantiate(ids));
        }
    }

    private ArrayList<String> getIdentifiers() {
        return getIdentifiersForRegex(name.toString(), "[A|V|W]([0-9]+)");
    }

    private ArrayList<String> getIdentifiersForRegex(String macro, String regex) {
        final ArrayList<String> identifiers = new ArrayList<String>();

        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(macro);

        while (m.find()) {
            String id = m.group(0);
            if (identifiers.indexOf(id) < 0) {
                identifiers.add(id);
            }
        }
        return identifiers;
    }

    public String getNameForDatabase() {
        return name.getNameForDatabase();
    }

    public String getFullName() {
        return name.getFullName();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("-----------------------------------\n");
        sb.append("MACRO NAME: " + name);
        sb.append("\n-----------\n");
        sb.append(pseudoProgram.toString());

        return sb.toString();
    }
}
