/**
 * S-Sigma Language Interpreter
 * <p>
 * Copyright (C) 2015  Gabriel Cerceau.
 * Email: debianr3@gmail.com
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.lengform.ssigma.macro;

import java.util.Hashtable;
import java.util.Set;
import java.util.TreeSet;

public class MacroDatabase {

    private final Hashtable<String, Macro> table = new Hashtable<String, Macro>();
    private final Set<String> names = new TreeSet<String>();

    public void add(Macro m) {
        final String name = m.getNameForDatabase();
        if (table.contains(name)) {
            throw new IllegalArgumentException("Macro with name: " + m + ", already exist!");
        }
        table.put(name, m);
        names.add(m.getFullName());
    }

    public Set<String> getFullMacroNames() {
        return names;
    }

    public Macro getMacro(String macroNameInDatabase) {
        return table.get(macroNameInDatabase);
    }

    public void add(MacroDatabase other) {
        for (Macro m : other.table.values()) {
            add(m);
        }
        names.addAll(other.names);
    }
}
